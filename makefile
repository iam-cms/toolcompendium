SOURCEFILE ?= example.xml # Can be overwritten from the commandline using `make SOURCEFILE=data.xml`

BUILD_DIR ?= build
PDF_BUILD_DIR = $(BUILD_DIR)/pdf
HTML_BUILD_DIR = $(BUILD_DIR)/html

SOURCE_DIR = src
PDF_SOURCE_DIR = $(SOURCE_DIR)/pdf
HTML_SOURCE_DIR = $(SOURCE_DIR)/html

HTML_PAGE_TITLE ?= Online Documentation

ifdef CUSTOMCSS
CUSTOMCSS_COPY=cp $(CUSTOMCSS) $(HTML_BUILD_DIR)/
endif

pdf:
	mkdir -p $(PDF_BUILD_DIR)
	xsltproc\
	  --stringparam preamble $(PDF_SOURCE_DIR)/preamble.tex\
	  --stringparam titlepage $(PDF_SOURCE_DIR)/titlepage.tex\
	  --stringparam licensepage $(PDF_SOURCE_DIR)/licensepage.tex\
	  --stringparam introduction $(PDF_SOURCE_DIR)/introduction.tex\
	  --stringparam footer $(PDF_SOURCE_DIR)/footer.tex\
	  $(PDF_SOURCE_DIR)/toolcompendiumLatex.xsl $(SOURCEFILE) > $(PDF_BUILD_DIR)/toolcompendium.tex
	pdflatex -interaction nonstopmode -shell-escape -output-directory $(PDF_BUILD_DIR)/ $(PDF_BUILD_DIR)/toolcompendium.tex
	makeindex $(PDF_BUILD_DIR)/toolcompendium.idx
	pdflatex -interaction nonstopmode -shell-escape -output-directory $(PDF_BUILD_DIR)/ $(PDF_BUILD_DIR)/toolcompendium.tex

html:
	mkdir -p $(HTML_BUILD_DIR)
	cp $(HTML_SOURCE_DIR)/toolcompendium.css $(HTML_BUILD_DIR)/
	$(CUSTOMCSS_COPY)
	cp $(HTML_SOURCE_DIR)/toolcompendium.js $(HTML_BUILD_DIR)/
	xsltproc \
	  --stringparam title '$(HTML_PAGE_TITLE)'\
	  --stringparam stylesheet toolcompendium.css\
	  --stringparam stylesheet_custom "$(CUSTOMCSS)"\
	  --stringparam javascript toolcompendium.js\
	  src/html/toolcompendiumHTML.xsl $(SOURCEFILE) > $(HTML_BUILD_DIR)/index.html

clean:
	rm -f $(PDF_BUILD_DIR)/*
	rm -f $(HTML_BUILD_DIR)/*
