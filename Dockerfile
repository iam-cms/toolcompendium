FROM debian:bookworm-slim

ARG USER_ID=1000
ARG GROUP_ID=1000

ARG app_path=/home/user/
ARG target=html

RUN apt-get update
RUN apt-get -y install make xsltproc sudo
RUN if [ "$target" = "pdf" ] ; then apt-get -y install texlive-full ; fi

# To avoid that files are being created as root when running the container.
# (otherwise this can cause trouble in a gitlab ci setup because such files
#  and folders can not be removed by the runner)
# Set USER_ID and GROUP_ID of the user which will run the container.
RUN addgroup --gid $GROUP_ID user
RUN adduser --disabled-login --gecos '' --uid $USER_ID --gid $GROUP_ID user
RUN passwd -d user
RUN usermod -aG sudo user
# To avoid the message when using sudo for the first time
RUN touch /var/lib/sudo/lectured/user
USER user

WORKDIR $app_path

COPY --chown=user:user . $app_path

ENV app_path=$app_path
ENV target=$target
ENV sourcefile=example.xml
ENV build_dir=build
ENV output_dir=shared
ENV html_page_title="Online Documentation"

# Use sudo to copy the files afterwards to work around permission issues
# (binding volumes in a non-root docker setup changes the ownership of
#  concerned folders inside the container to # root:root,
#  which causes permission denied when trying to write)
CMD make -C $app_path SOURCEFILE=$sourcefile CUSTOMCSS=$custom_css BUILD_DIR=$build_dir HTML_PAGE_TITLE="$html_page_title" $target; sudo cp -vR $build_dir/* $output_dir/; sudo chown user:user -R $output_dir
