# toolcompendium

Toolcompendium is a collection of XSLT scripts to generate documentation from XML tool descriptions.

Copyright (C) 2019  IAM-CMS


# How to use
## Basic usage with make
Building the PDF documentation:
```bash
make SOURCEFILE=data.xml pdf
```
Building the HTML documentation:
```bash
make SOURCEFILE=data.xml html
```
To provide custom CSS when building the HTML documentation use the optional parameter `CUSTOMCSS`:
```bash 
make SOURCEFILE=data.xml CUSTOMCSS=custom.css html
```

Replace `data.xml` with the name of your XML file containing tool descriptions.


## Usage with docker
There are two different docker containers for generating either pdf or html.
This is helpful because the container for generating the pdf needs texlive and therefore is much bigger than the one for html.

When using the docker containers, input files should be mapped to make them available in the container.
Either map each input file separately or use a shared folder.
Mapping the containers build folder allows to access the generated files easily.

In the examples below, the input files should be located in the current directory and the output of the containers can be found in the folder `./build` after `docker run` finished.
The examples use the pre-build images in the registry of this project (`docker.opencarp.org/iam-cms/toolcompendium`).
Alternatively you may also build the containers yourself (see below).

Example for building the PDF documentation:
```bash
docker run\
    -v $(pwd)/data.xml:/container/data.xml\
    -v $(pwd)/build:/container/build/\
    --env sourcefile=data.xml\
    docker.opencarp.org/iam-cms/toolcompendium/pdf
```
Example for building the HTML documentation:
```bash
docker run\
    --volume $(pwd)/data.xml:/container/data.xml\
    --volume $(pwd)/build:/container/build/html/\
    --env sourcefile=data.xml\
    docker.opencarp.org/iam-cms/toolcompendium/html
```
Example for building the HTML documentation providing a custom css file (using a shared folder containing the input files):
```bash
docker run\
    --volume $(pwd)/shared:/container/shared\
    --volume $(pwd)/build:/container/build/\
    --env sourcefile=shared/data.xml\
    --env custom_css=shared/custom.css\
    docker.opencarp.org/iam-cms/toolcompendium/html
```

### Building the docker images
The Dockerfile of this project allows to build two different docker containers for generating either pdf or html.

After cloning the project, the containers can be build with the following commands from within the project's root directory:
```bash
sudo docker build . --build-arg target=html -t toolcompendium-html
sudo docker build . --build-arg target=pdf -t toolcompendium-pdf
```

### Pulling the docker images from the registry

```bash
docker pull docker.opencarp.org/iam-cms/toolcompendium/html
docker pull docker.opencarp.org/iam-cms/toolcompendium/pdf
```

## Authors

- Jorge Sánchez	<jorge.arciniegas@kit.edu>
- Michael Selzer	<michael.selzer@hs-karlsruhe.de>
- Philipp Zschumme	<philipp.zschumme@kit.edu>

## Funding
This project is funded by the [DFG](https://www.dfg.de/) as part of the project SuLMaSS (Sustainable Lifecycle Management for Scientific Software)
