<?xml version="1.0"?>
<!--
  toolcompendium: xsl transformation to generate documention for command line tools.
  Copyright (C) 2019  IAM-CMS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" omit-xml-declaration="yes"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
              doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
              indent="yes"/>

  <xsl:template match="/">
   <html>
    <head>
     <title><xsl:value-of select="$title"/></title>
     <link rel="stylesheet" type="text/css" href="{$stylesheet}" />
     <xsl:if test="string-length($stylesheet_custom) &gt; 0">
     <link rel="stylesheet" type="text/css" href="{$stylesheet_custom}" />
     </xsl:if>
    </head>
    <body>

      <div class="tc-content" id="tc-top">
        <h1 align="center"><xsl:value-of select="$title"/></h1>

        <input type="checkbox" id="tc-responsive-nav" xmlns="http://www.w3.org/TR/html5/">
          <label for="tc-responsive-nav" class="tc-responsive-nav-label" onclick="tcTransformHamburger(this)">
            <div class="tc-container">
              <div class="tc-bar1"></div>
              <div class="tc-bar2"></div>
              <div class="tc-bar3"></div>
            </div>
            Navigation
            </label>
        </input>

        <nav>
          <xsl:apply-templates select="/programs/section" mode="toc"/>
          <ul class="tc-floatright">
            <input type="search" id="tc-searchInput" onkeyup="tcSearchFilter()" placeholder="Search.." title="Type in a search word" />
          </ul>
        </nav>

        <!-- Back to top button -->
        <a id="tc-gotop" href="#" onclick="window.scrollTo(0,0)" title="Go to the top"></a>

        <xsl:apply-templates select="programs/*[not(self::param) and not(self::struct-ref)]"/>
      </div>

      <script type="text/javascript" src="{$javascript}"></script>

    </body>
   </html>
  </xsl:template>

<!-- templates with mode=toc create the content menu -->
<xsl:template match="section[program|section|struct-ref[@name]]" mode="toc">
  <ul class="tc-floatleft">
    <xsl:variable name="anzahl" select="count(program|section|struct-ref[@name])" />
    <xsl:if test="$anzahl = 0">
      <li><a onclick="tcOpenSection(&quot;{@name}&quot;)" href="#{@name}"><xsl:value-of select="@name"/></a></li>
    </xsl:if>
    <xsl:if test="$anzahl &gt;= 1">
      <li class="tc-submenu"><a onclick="tcOpenSection(&quot;{@name}&quot;)" href="#{@name}"><xsl:value-of select="@name"/></a>
      <ul>
      <xsl:for-each select="program|section|struct-ref[@name]">
          <li><a onclick="tcOpenSection(&quot;{@name}&quot;)" href="#{@name}"><xsl:value-of select="@name"/></a></li>
      </xsl:for-each>
      </ul>
      </li>
    </xsl:if>
  </ul>
</xsl:template>

  <xsl:template match="title">
    <div class="tc-category">
      <h2 align="center"><a href="" name="{@name}"><xsl:value-of select="substring-after(@name,'/')"/></a></h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="section[program|section|struct-ref]">
    <div class="tc-searchable tc-section">
      <button class="tc-accordion">
        <a id="{@name}" class="tc-section-name" href="#{@name}" name="{@name}">
          <xsl:value-of select="@name"/>
        </a>
      </button>
      <div class="tc-panel" style="overflow-x:auto;">
        <xsl:apply-templates select="*[not(self::param)]" />
      </div>
    </div>
  </xsl:template>

  <xsl:template match="description">
    <div class="description">      
      <xsl:if test="boolean(.)">      
        <xsl:call-template name="lf2br">
          <xsl:with-param name="StringToTransform" select="."/>
        </xsl:call-template>
      </xsl:if>    
    </div>
  </xsl:template>

  <xsl:template match="program">
    <xsl:param name="program_description" select="description" />
    <xsl:call-template name="programInternal">
      <xsl:with-param name="description" select="$program_description"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="programInternal">
    <xsl:param name="description" />
    <xsl:param name="father" />
    <xsl:param name="override-name" />

    <div class="tc-searchable tc-program">
      <button class="tc-accordion">
        <xsl:choose>
          <xsl:when test="$override-name">
            <a id="{$override-name}" class="tc-program-name" href="#{$override-name}" name="{$override-name}">
              <xsl:value-of select="$override-name"/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <a id="{@name}" class="tc-program-name" href="#{@name}" name="{@name}">
              <xsl:value-of select="@name"/>
            </a>
          </xsl:otherwise>
        </xsl:choose>
      </button>
      <div class="tc-panel" style="overflow-x:auto;">
      <center>
        <table class="tc-progtable" border="0" align="center" width="930px">
          <xsl:if test="$description">
          <!-- program description -->
          <tr>
            <td class="tc-left-column" valign="top">Description</td>
            <td class="tc-right-column">
              <xsl:call-template name="lf2br">
                <xsl:with-param name="StringToTransform" select="$description"/>
              </xsl:call-template>
            </td>
          </tr>
          </xsl:if>
          <!-- program attributes -->
          <xsl:apply-templates select="@*[name() != 'name' and name() != 'description']"/>
          <!-- program parameters -->
          <tr class="tc-header">
            <td colspan="2">
              <h4>Parameters</h4>
            </td>
          </tr>
          <xsl:apply-templates select="param">
            <xsl:with-param name="father" select="$father"/>
          </xsl:apply-templates>
        </table>
        <xsl:apply-templates select="param-ref">
          <xsl:with-param name="father" select="$father"/>
        </xsl:apply-templates>
      </center>
    </div>
    </div>
  </xsl:template>

  <xsl:template match="param-ref">
    <xsl:param name="father" />

    <!-- resolve the reference -->
    <xsl:variable name="struct_ref_name" select="@name" />
    <xsl:variable name="struct_ref_father_name" select="$father" />
    <xsl:variable name="struct_description" select="description" />

    <xsl:apply-templates select="/programs/struct-ref[@name=$struct_ref_name]">
      <xsl:with-param name="father_name" select="$struct_ref_father_name" />
      <xsl:with-param name="description" select="$struct_description" />
      <xsl:with-param name="override-name" select="@as-param" /> <!-- to override with which name the struct-ref is shown -->
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="struct-ref">
    <xsl:param name="father_name" />
    <xsl:param name="description" />
    <xsl:param name="override-name" />

    <xsl:if test="parent::section">
      <!-- resolve the reference -->
      <xsl:variable name="struct_ref_name" select="@name" />
      <xsl:variable name="struct_ref_father_name" select="@father-name" />
      <xsl:variable name="struct_description" select="description" />

      <xsl:apply-templates select="/programs/struct-ref[@name=$struct_ref_name]">
        <!-- call this template again to print the program -->
        <xsl:with-param name="father_name" select="$struct_ref_father_name" />
        <xsl:with-param name="description" select="$struct_description" />
        <xsl:with-param name="override-name" select="$override-name" /> <!-- to override with which name the struct-ref is shown -->
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="parent::programs">
      <!-- print referenced program -->
      <xsl:call-template name="programInternal">
        <!-- copying the description of the <struct-ref> over to the <program> -->
        <xsl:with-param name="description" select="$description" />
        <xsl:with-param name="father" select="$father_name" />
        <xsl:with-param name="override-name" select="$override-name" /> <!-- to override with which name the struct-ref is shown -->
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="param">
    <xsl:param name="father" />
    <xsl:variable name="short" select="@*[name() = 'char']"/>
    <!-- param header -->
    <tr class="tc-header">
      <td colspan="2">
        <xsl:apply-templates select="@*[name() = 'char']"/>
        <xsl:apply-templates select="@*[name() = 'name']"/>
        <xsl:apply-templates select="@*[name() = 'type']"/>

        <xsl:variable name="required" select="@*[name() = 'required']"/>
        <xsl:if test="$required = 'true'">
          <b><xsl:apply-templates select="@*[name() = 'required']"/></b>
        </xsl:if>
        <xsl:if test="not($required = 'true')">
          <xsl:if test="not(@*[name() = 'type'] = 'flag') and not(@*[name() = 'type'] = 'plot')">
            <xsl:apply-templates select="@*[name() = 'default']"/>
          </xsl:if>
        </xsl:if>
      </td>
    </tr>
    <!-- param description -->
    <tr>
      <td class="tc-left-column" valign="top">Description</td>
      <td class="tc-right-column">
        <xsl:call-template name="lf2br">
          <xsl:with-param name="StringToTransform" select="description"/>
        </xsl:call-template>
      </td>
    </tr>
    <xsl:call-template name="unit">      
    </xsl:call-template>
    <xsl:call-template name="defaultValue">      
    </xsl:call-template>    
    <xsl:apply-templates select="@*[name() = 'interval']"/>
    <xsl:call-template name="validation">
      <xsl:with-param name="father" select="$father" />
    </xsl:call-template>
    <xsl:call-template name="relations">
      <xsl:with-param name="father" select="$father" />
    </xsl:call-template>

  </xsl:template>

  <xsl:template name="relations">
    <xsl:param name="father" />
    <xsl:if test="boolean(relation)">
      <!-- group all relations by type -->
      <xsl:variable name="required_parameters" select="relation[(@type = 'require' or not(boolean(@type))) and boolean(@to-name)]" />
      <xsl:variable name="required_groups" select="relation[(@type = 'require' or not(boolean(@type))) and boolean(@to-group)]" />
      <xsl:variable name="excluded_parameters" select="relation[@type = 'exclude' and boolean(@to-name)]" />
      <xsl:variable name="excluded_groups" select="relation[(@type = 'require' or not(boolean(@type))) and boolean(@to-group)]" />

      <!-- print relation groups, each with a short title -->
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$required_parameters" />
        <xsl:with-param name="title">required parameter</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$required_groups" />
        <xsl:with-param name="title">required group</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$excluded_parameters" />
        <xsl:with-param name="title">excluded parameter</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$excluded_groups" />
        <xsl:with-param name="title">excluded group</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>


  <xsl:template name="list_relations">
    <xsl:param name="relations" />
    <xsl:param name="title" /> <!-- singular, plural 's' will be added if more than one relation! -->
    <xsl:param name="father" />

    <xsl:if test="count($relations) &gt; 0">
      <tr>
        <td class="tc-left-column">
          <xsl:value-of select="$title" />
          <xsl:choose>
            <xsl:when test="count($relations) &gt; 1">
              <xsl:text>s:</xsl:text><!-- plural s -->
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>:</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="tc-right-column">
          <ul class="tc-list-relations">
            <xsl:for-each select="$relations">
              <xsl:variable name="to_name">
                <xsl:call-template name="try-search-and-replace">
                  <xsl:with-param name="str" select="@to-name"/>
                  <xsl:with-param name="search" select="'[[Father]]'"/>
                  <xsl:with-param name="replace" select="$father"/>
                </xsl:call-template>
              </xsl:variable>
              <li>
                <xsl:call-template name="lf2br">
                  <xsl:with-param name="StringToTransform" select="$to_name" />
                </xsl:call-template>
              </li>
            </xsl:for-each>
          </ul>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template name="unit">
    <xsl:if test="boolean(unit)">
      <tr>
        <td class="tc-left-column">Unit</td>
        <td class="tc-right-column">
          <xsl:call-template name="lf2br">
          <xsl:with-param name="StringToTransform" select="unit"/>
          </xsl:call-template>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template name="defaultValue">
    <xsl:if test="boolean(default)">
      <tr>
        <td class="tc-left-column">Default value</td>
        <td class="tc-right-column">
          <xsl:call-template name="lf2br">
          <xsl:with-param name="StringToTransform" select="default"/>
          </xsl:call-template>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template name="validation">
    <xsl:param name="father" />
    <xsl:if test="boolean(validation)">
      <tr>
        <td class="tc-left-column">Validation</td>
        <td class="tc-right-column">
          <xsl:choose>
            <xsl:when test="validation/@type = 'options'">
              <p>Possible values are</p>
              <ul>
                <xsl:for-each select="validation/option">
                  <li>
                    <xsl:call-template name="lf2br">
                      <xsl:with-param name="StringToTransform">
                        <xsl:call-template name="try-search-and-replace">
                          <xsl:with-param name="str" select="@value"/>
                          <xsl:with-param name="search" select="'[[Father]]'"/>
                          <xsl:with-param name="replace" select="$father" />
                        </xsl:call-template>
                      </xsl:with-param>
                    </xsl:call-template>
                    <xsl:text>: </xsl:text>
                    <xsl:call-template name="lf2br">
                      <xsl:with-param name="StringToTransform" select="@name"/>
                    </xsl:call-template>
                  </li>
                </xsl:for-each>
              </ul>
            </xsl:when>
            <xsl:when test="validation/@type = 'range'">
              <xsl:text>Value must be</xsl:text>
              <xsl:if test="boolean(validation/min) and boolean(validation/max)">
                <xsl:text> between </xsl:text>
                <xsl:call-template name="lf2br">
                  <xsl:with-param name="StringToTransform">
                    <xsl:call-template name="try-search-and-replace">
                      <xsl:with-param name="str" select="validation/min"/>
                      <xsl:with-param name="search" select="'[[Father]]'"/>
                      <xsl:with-param name="replace" select="$father" />
                    </xsl:call-template>
                  </xsl:with-param>
                </xsl:call-template>
                <xsl:text> and </xsl:text>
                <xsl:call-template name="lf2br">
                  <xsl:with-param name="StringToTransform">
                    <xsl:call-template name="try-search-and-replace">
                      <xsl:with-param name="str" select="validation/max"/>
                      <xsl:with-param name="search" select="'[[Father]]'"/>
                      <xsl:with-param name="replace" select="$father" />
                    </xsl:call-template>
                  </xsl:with-param>
                </xsl:call-template>
              </xsl:if>
              <xsl:if test="boolean(validation/min) and not(boolean(validation/max))">
                <xsl:text> greater than </xsl:text>
                <xsl:call-template name="lf2br">
                  <xsl:with-param name="StringToTransform">
                    <xsl:call-template name="try-search-and-replace">
                      <xsl:with-param name="str" select="validation/min"/>
                      <xsl:with-param name="search" select="'[[Father]]'"/>
                      <xsl:with-param name="replace" select="$father" />
                    </xsl:call-template>
                  </xsl:with-param>
                </xsl:call-template>
              </xsl:if>
              <xsl:if test="not(boolean(validation/min)) and boolean(validation/max)">
                <xsl:text> smaller than </xsl:text>
                <xsl:call-template name="lf2br">
                  <xsl:with-param name="StringToTransform">
                    <xsl:call-template name="try-search-and-replace">
                      <xsl:with-param name="str" select="validation/max"/>
                      <xsl:with-param name="search" select="'[[Father]]'"/>
                      <xsl:with-param name="replace" select="$father" />
                    </xsl:call-template>
                  </xsl:with-param>
                </xsl:call-template>
              </xsl:if>
            </xsl:when>
          </xsl:choose>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template name="lf2br">
    <!-- import $StringToTransform -->
    <xsl:param name="StringToTransform"/>
      <xsl:choose>
        <!-- string contains linefeed -->
        <xsl:when test="contains($StringToTransform,'&#xA;')">
          <!-- output substring that comes before the first linefeed -->
          <!-- note: use of substring-before() function means        -->
          <!-- $StringToTransform will be treated as a string,       -->
          <!-- even if it is a node-set or result tree fragment.     -->
          <!-- So hopefully $StringToTransform is really a string!   -->
          <xsl:value-of select="substring-before($StringToTransform,'&#xA;')"/>
          <!-- by putting a 'br' element in the result tree instead  -->
          <!-- of the linefeed character, a <br> will be output at   -->
          <!-- that point in the HTML                                -->
          <br/>
          <!-- repeat for the remainder of the original string -->
          <xsl:call-template name="lf2br">
          <xsl:with-param name="StringToTransform">
            <xsl:value-of select="substring-after($StringToTransform,'&#xA;')"/>
          </xsl:with-param>
        </xsl:call-template>
        </xsl:when>
           <!-- string does not contain newline, so just output it -->
        <xsl:otherwise>
          <xsl:value-of select="$StringToTransform"/>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>

  <xsl:template name="try-search-and-replace">
    <xsl:param name="str"/>
    <xsl:param name="search"/>
    <xsl:param name="replace"/>

    <xsl:choose>
      <xsl:when test="$replace != ''">
        <xsl:call-template name="search-and-replace">
          <xsl:with-param name="str" select="$str"/>
          <xsl:with-param name="search" select="$search"/>
          <xsl:with-param name="replace" select="$replace"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$str"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="search-and-replace">
    <xsl:param name="str"/>
    <xsl:param name="search"/>
    <xsl:param name="replace"/>
    <xsl:choose>
      <xsl:when test="contains($str, $search)">
        <xsl:value-of select="substring-before($str, $search)"/>
        <xsl:value-of select="$replace"/>
        <xsl:call-template name="search-and-replace">
          <xsl:with-param name="str" select="substring-after($str, $search)"/>
          <xsl:with-param name="search" select="$search"/>
          <xsl:with-param name="replace" select="$replace"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$str"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

<!-- matches all attributes -->
  <xsl:template match="@*">
   <xsl:choose>
    <xsl:when test="name() = 'name'">
     <xsl:choose>
<!--      no regular expressions supported by xslt1.0! -> arg{0-9}1-->
      <xsl:when test=". = 'arg0'" />
      <xsl:when test=". = 'arg1'" />
      <xsl:when test=". = 'arg2'" />
      <xsl:when test=". = 'arg3'" />
      <xsl:when test=". = 'arg4'" />
      <xsl:when test=". = 'arg5'" />
      <xsl:when test=". = 'arg6'" />
      <xsl:when test=". = 'arg7'" />
      <xsl:when test=". = 'arg8'" />
      <xsl:when test=". = 'arg9'" />
      <xsl:otherwise>
       <xsl:text></xsl:text><xsl:value-of select="."/><xsl:text> </xsl:text>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="name() = 'char'">
     <xsl:text>-</xsl:text><xsl:value-of select="."/><xsl:text> </xsl:text>
    </xsl:when>
    <xsl:when test="name() = 'type'">
      <xsl:if test="not(. = 'flag')">
        <xsl:text>&lt;</xsl:text><xsl:value-of select="."/><xsl:text>&gt;</xsl:text>
      </xsl:if>
    </xsl:when>
    <xsl:when test="name() = 'required'">
      <xsl:if test=". = 'true'">
        <xsl:text> (</xsl:text><xsl:value-of select="name()"/><xsl:text>)</xsl:text>
      </xsl:if>
    </xsl:when>
    <xsl:when test="name() = 'default'">
      <xsl:text> (</xsl:text><xsl:value-of select="name()"/>=<xsl:value-of select="."/><xsl:text>)</xsl:text>
    </xsl:when>
    <xsl:when test="name() = 'istested'">
      <tr>
        <td width="tc-left-column">test status</td>
      <xsl:if test="number(.) &gt; -1">
        <xsl:variable name="color">
          <xsl:choose>
            <xsl:when test="number(.) &lt; 40">
              background-color:red
            </xsl:when>
            <xsl:when test="number(.) &lt; 90">
              background-color:yellow
            </xsl:when>
            <xsl:when test="number(.) &lt; 100">
              background-color:green
            </xsl:when>
            <xsl:otherwise>background-color:white</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <td class="tc-right-column" style="{$color}"><xsl:value-of select="."/>%</td>
      </xsl:if>
      <xsl:if test="number(.) &lt; 0">
        <td class="tc-right-column" style="background-color:darkred;color:white">This tool does not work or has serious bugs!</td>
      </xsl:if>
      </tr>
    </xsl:when>
    <xsl:otherwise>
     <tr>
       <td class="tc-left-column"><xsl:value-of select="name()"/></td>
       <td class="tc-right-column">
         <xsl:call-template name="lf2br">
           <xsl:with-param name="StringToTransform" select="."/>
         </xsl:call-template>
       </td>
     </tr>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:template>

  <xsl:template match="*"/>
  <xsl:template match="text()" mode="toc"/>
</xsl:transform>
