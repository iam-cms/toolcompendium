<!-- Accordion -->
var acc = document.getElementsByClassName("tc-accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("tc-active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = "none";
    }
  });
}


function tcPerformClick(el){
  const etype = "click";
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}

function tcOpenSection(name) {
  const searchables = document.getElementsByClassName("tc-searchable");
  var subsectionFound = false;
  for (let i = 0; i < searchables.length; i++) {
    const searchable = searchables[i];
    const links = searchable.getElementsByTagName("a");
    for (let j = 1; j < links.length; j++) {
      const link = links[j];
      if (link.getAttribute("name") === name) {
        tcPerformClick(links[0]); // open the containing section
        tcPerformClick(link); // open the subsection
        return;
      }
    }
    if (links[0].getAttribute("name") === name) {
      tcPerformClick(links[0]); // open the section
      return;
    }
  }
}

function tcSearchFilter() {
  const input = document.getElementById("tc-searchInput");
  const filter = input.value.toUpperCase();
  applySearchFilter(filter);
}

function matchFilters(content, filters) {
  let result = true;
  for (let i = 0; i < filters.length; i++) {
    let filter = filters[i];
    if (filter === "") continue; // ignore empty filter strings
    result = result && (content.toUpperCase().indexOf(filter.toUpperCase()) > -1);
    if (!result) break;
  }
  return result;
}

function sectionNameMatches(searchable, filter_strings) {
  let sectionName = searchable.getElementsByClassName("tc-section-name")[0];
  if (!sectionName) return false;
  let sectionNameText = sectionName.textContent || sectionName.innerText;
  return matchFilters(sectionNameText, filter_strings);
}

function descriptionMatches(searchable, filter_strings) {
  let description = searchable.getElementsByClassName("description")[0];
  if (!description) return false;
  let descriptionText = description.textContent || description.innerText;
  return matchFilters(descriptionText, filter_strings);
}

function programNameMatches(searchable, filter_strings) {
  let programName = searchable.getElementsByClassName("tc-program-name")[0];
  if (!programName) return false;
  let programNameText = programName.textContent || programName.innerText;
  return matchFilters(programNameText, filter_strings);
}

function tableEntriesMatch(searchable, filter_strings) {
  let table = searchable.getElementsByClassName("tc-progtable");
  if (!table) return false;

  let result = false;
  let tableRow = table[0].getElementsByTagName("tr");
  for (let i = 0; i < tableRow.length; i++) {
    let columns = tableRow[i].getElementsByTagName("td");
    let relevantColumn = columns[1]; // only include the second column which holds actual information
    if (relevantColumn) {
      let columnContent = relevantColumn.textContent || relevantColumn.innerText;
      if (matchFilters(columnContent, filter_strings)) {
        result = true;
      }
    }
  }
  return result;
}

function applySearchFilter(filter) {
  <!-- Search through the table -->
  let filter_strings = filter.split(" ");
  let searchables = document.getElementsByClassName("tc-searchable");
  let searchables_with_matched_parent = [];
  for (let i = 0; i < searchables.length; i++) {
    let searchable = searchables[i];
    if (sectionNameMatches(searchable, filter_strings)
        || descriptionMatches(searchable, filter_strings)
        || programNameMatches(searchable, filter_strings)
        || tableEntriesMatch(searchable, filter_strings)) {
      searchables[i].style.display = "";

      for (const subsearchable of searchables[i].getElementsByClassName("tc-searchable")) {
        searchables_with_matched_parent.push(subsearchable);
      }
    } else {
      searchables[i].style.display = "none";
    }
  }

  // make searchables visible when their parent matched the filter (to show all categories in full, if they match)
  for (const subsearchable of searchables_with_matched_parent) {
    subsearchable.style.display = "";
  }

  // make all sections visible that contain at least one visible searchable
  let categories = document.querySelectorAll(".tc-section, .tc-program"); // tc-program can also have subsections since <param-ref> was introduced
  for (let i = 0; i < categories.length; i++) {
    let searchables = categories[i].getElementsByClassName("tc-searchable");
    let k = 0;
    for (let j = 0; j < searchables.length; j++) {
      if (searchables[j].style.display === "") {
        k++;
      }
    }
    if (k > 0) {
      categories[i].style.display = "";
    }
  }
}

<!-- Back to top -->
var gotop = document.getElementById('tc-gotop');

window.onscroll = function() {
  if ((window.parent.pageYOffset || window.parent.document.documentElement.scrollTop) > 250) {
    gotop.classList.add("tc-show");
  } else {
    gotop.classList.remove("tc-show");
  }
};

<!-- Transform Hamburger button -->
function tcTransformHamburger(x) {
  x.classList.toggle("tc-change");
}
